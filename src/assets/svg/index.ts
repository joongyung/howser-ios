import Memo from './memo.svg';
import Schedule from './schedule.svg';
import Close from './close.svg';
import Arrow from './arrow.svg';
import Menu from './menu.svg';

export {Memo, Schedule, Close, Arrow, Menu};
