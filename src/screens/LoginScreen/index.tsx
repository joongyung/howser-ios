import React, {useRef} from 'react'
import {Alert} from 'react-native'
import {ScreenBaseProps} from '..'
import Input, {InputHandle} from '../../components/Input'
import {login as Login} from '../../apis/account/login'
import DefaultButton from '../../components/Button/DefaultButton'
import styles from './styles'
import ScreenLayout from '../../components/ScreenLayout'
import {setStorage} from '../../utils/Storage/useStorage'

interface Props extends ScreenBaseProps {}

function LoginScreen({}: Props) {
    const idRef = useRef<InputHandle>(null)
    const pwRef = useRef<InputHandle>(null)

    const login = async () => {
        const id = idRef.current?.getValue()
        const pw = pwRef.current?.getValue()

        if (!id || !pw) {
            Alert.alert('아이디와 비밀번호를 모두 입력해주세요.')
            return
        }

        const srl = await Login(
            idRef.current!.getValue(),
            pwRef.current!.getValue()
        )

        if (srl) {
            setStorage('userSrl', srl)
        }
    }

    return (
        <ScreenLayout>
            <Input ref={idRef} style={styles.input} placeholder="ID" />
            <Input
                ref={pwRef}
                style={styles.input}
                placeholder="PW"
                textContentType="password"
                secureTextEntry
            />
            <DefaultButton title="로그인" onPress={login} />
        </ScreenLayout>
    )
}

export default LoginScreen
