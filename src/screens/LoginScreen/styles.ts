import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    height: '100%',
    fontSize: 40,
    fontWeight: '700',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 14,
  },
  input: {
    backgroundColor: '#FFF',
    padding: 16,
    marginBottom: 5,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 12,
    color: '#000',
  },
});
