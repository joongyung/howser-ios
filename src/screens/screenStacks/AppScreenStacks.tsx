import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import {navigationRef} from '../useNavigator'
import DrawerNavigatorStacks from './DrawerNavigatorStacks'

import {
    AUTH_SCREENS,
    AUTH_SCREENS_KEYS,
    UNAUTH_SCREENS,
    UNAUTH_SCREENS_KEYS,
} from '.'
import {useStorage} from '../../utils/Storage/useStorage'

const Stack = createNativeStackNavigator()

function AppScreenStacks() {
    const [userSrl] = useStorage('userSrl')

    return (
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator>
                {/* NavigationContainer stacks */}
                {userSrl ? (
                    <Stack.Screen
                        name="HOME"
                        options={{
                            headerShown: false,
                        }}>
                        {() => <DrawerNavigatorStacks />}
                    </Stack.Screen>
                ) : null}

                {/* screen stacks */}
                {userSrl
                    ? AUTH_SCREENS_KEYS.map((key) => (
                          <Stack.Screen
                              key={key}
                              name={AUTH_SCREENS[key].name}
                              getComponent={() => AUTH_SCREENS[key].component}
                          />
                      ))
                    : UNAUTH_SCREENS_KEYS.map((key) => (
                          <Stack.Screen
                              key={key}
                              name={UNAUTH_SCREENS[key].name}
                              getComponent={() => UNAUTH_SCREENS[key].component}
                          />
                      ))}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppScreenStacks
