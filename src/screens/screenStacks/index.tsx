import React from 'react';
import Icon from '../../components/Icon';
import HomeScreen from '../HomeScreen';
import LoginScreen from '../LoginScreen';
import LogoutScreen from '../LogoutScreen';
import GroupListScreen from '../GroupListScreen';
import SplashScreen from '../SplashScreen';
import TestScreen from '../TestScreen';

// 사용자가 로그인했을 시에 볼 수 있는 화면들
export const AUTH_SCREENS = {
  LogoutScreen: {
    name: '로그아웃 화면',
    component: LogoutScreen,
  },
  GroupListScreen: {
    name: '그룹리스트 화면',
    component: GroupListScreen,
  },
  SplashScreen: {
    name: '스플래시 화면',
    component: SplashScreen,
  },
  TestScreen: {
    name: '테스트 화면',
    component: TestScreen,
  },
} as const;

// 사용자가 비로그인시에 볼 수 있는 화면들
export const UNAUTH_SCREENS = {
  LoginScreen: {
    name: '로그인 화면',
    component: LoginScreen,
  },
} as const;

// 왼쪽 네비게이션 메뉴에 표시할 화면들
export const DRAWER_SCREENS = {
  HomeScreen: {
    nav: '홈',
    name: '홈',
    component: HomeScreen,
    icon: () => <Icon name="Schedule" />,
  },
  GroupListScreen: {
    nav: '그룹 리스트',
    name: '그룹리스트 화면',
    component: GroupListScreen,
    icon: () => <Icon name="Memo" />,
  },
} as const;

// key 값을 배열로 추출
export const AUTH_SCREENS_KEYS = Object.keys(
  AUTH_SCREENS,
) as (keyof typeof AUTH_SCREENS)[];

export const DRAWER_SCREENS_KEYS = Object.keys(
  DRAWER_SCREENS,
) as (keyof typeof DRAWER_SCREENS)[];

export const UNAUTH_SCREENS_KEYS = Object.keys(
  UNAUTH_SCREENS,
) as (keyof typeof UNAUTH_SCREENS)[];

// 화면이동을 위한 name 값 타입으로 지정
export type AUTH_SCREENS_NAME =
  typeof AUTH_SCREENS[keyof typeof AUTH_SCREENS]['name'];
export type UNAUTH_SCREENS_NAME =
  typeof UNAUTH_SCREENS[keyof typeof UNAUTH_SCREENS]['name'];
export type DRAWER_SCREENS_NAME =
  typeof DRAWER_SCREENS[keyof typeof DRAWER_SCREENS]['name'];
