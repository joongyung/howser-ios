import React from 'react'
import {createDrawerNavigator} from '@react-navigation/drawer'
import DrawerNavigator from '../../components/DrawerNavigator'
import {DRAWER_SCREENS, DRAWER_SCREENS_KEYS} from '.'

const Drawer = createDrawerNavigator()

function DrawerNavigatorStacks() {
    return (
        <Drawer.Navigator
            initialRouteName="HOME"
            drawerContent={(props) => <DrawerNavigator {...props} />}>
            {DRAWER_SCREENS_KEYS.map((key) => (
                <Drawer.Screen
                    key={key}
                    name={DRAWER_SCREENS[key].name}
                    getComponent={() => DRAWER_SCREENS[key].component}
                    options={{
                        drawerIcon: DRAWER_SCREENS[key].icon,
                    }}
                />
            ))}
        </Drawer.Navigator>
    )
}

export default DrawerNavigatorStacks
