import {useIsFocused} from '@react-navigation/core';
import React, {useEffect} from 'react';
import {SafeAreaView} from 'react-native';
import {ScreenBaseProps} from '..';

interface Props extends ScreenBaseProps {}

function SplashScreen({}: Props) {
  const isfocused = useIsFocused();
  useEffect(() => {}, [isfocused]);

  return <SafeAreaView></SafeAreaView>;
}

export default SplashScreen;
