import React from 'react'
import {Alert, Text} from 'react-native'
import {ScrollView} from 'react-native-gesture-handler'
import {ScreenBaseProps} from '..'
import Icon from '../../components/Icon'
import {useDefaultModal} from '../../components/Modal/useDefaultModal'
import ScreenLayout from '../../components/ScreenLayout'
import {useNavigator} from '../useNavigator'

interface Props extends ScreenBaseProps {}

function TestScreen({}: Props) {
    const {navigate, pop} = useNavigator()
    const {open: defaultModal} = useDefaultModal({
        desc: '안녕하세요? 저는 하우저입니다 :) \n 확인을 누르던 취소를 누르던~',
        input: {
            placeHolder: '입력하세요',
        },
    })

    const openDefaultModal = async () => {
        const result = await defaultModal()
        if (result.comfirm) {
            Alert.alert(`${result.data} 확인완료!`)
        }
    }

    const navLogout = () => {
        navigate('로그아웃 화면')
    }

    return (
        <ScreenLayout>
            <ScreenLayout.Header
                title="테스트"
                backType="XBUTTON"
                RightIcon={<Icon name="Schedule" width={20} height={20} />}
                onRightPress={navLogout}
            />
            <ScrollView>
                {[...Array(50).keys()].map((_, i) => (
                    <Text key={i}>{i}</Text>
                ))}
            </ScrollView>
            <ScreenLayout.Buttons
                prevTitle="취소"
                nextTitle="다음"
                onPrevPress={() => pop()}
                onNextPress={openDefaultModal}
            />
        </ScreenLayout>
    )
}

export default TestScreen
