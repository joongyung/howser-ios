import {
    DrawerActions,
    ParamListBase,
    useNavigation,
} from '@react-navigation/native'
import {createNavigationContainerRef} from '@react-navigation/native'
import {useMemo} from 'react'
import {StackActions} from '@react-navigation/native'
import {
    AUTH_SCREENS_NAME,
    DRAWER_SCREENS_NAME,
    UNAUTH_SCREENS_NAME,
} from './screenStacks'
import {NativeStackNavigationProp} from '@react-navigation/native-stack/lib/typescript/src/types'

type ScreenName = AUTH_SCREENS_NAME | UNAUTH_SCREENS_NAME | DRAWER_SCREENS_NAME

// Navigation Cotainer안에서만 사용가능.
export function useNavigator() {
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()

    return useMemo(() => {
        return {
            navigation,
            openDrawer() {
                navigation.dispatch(DrawerActions.openDrawer())
            },
            closeDrawer() {
                navigation.dispatch(DrawerActions.closeDrawer())
            },
            popToTop() {
                navigation.dispatch(StackActions.popToTop())
            },
            replace(screen: ScreenName, params?: any) {
                navigation.dispatch(StackActions.replace(screen, params))
            },
            pop(cnt?: number) {
                navigation.dispatch(StackActions.pop(cnt))
            },
            navigate(screen: ScreenName, params?: any) {
                navigation.navigate(screen, params)
            },
        }
    }, [navigation])
}

// Navigation Cotainer밖에서도 실행가능.
export const navigationRef = createNavigationContainerRef()
export function forceReplaceScreen(screen: ScreenName) {
    if (navigationRef.isReady()) {
        navigationRef.dispatch(StackActions.replace(screen))
    }
}
