import React from 'react'
import {Alert, Button} from 'react-native'
import {ScreenBaseProps} from '..'
import ScreenLayout from '../../components/ScreenLayout'
import {useUserGroups} from '../../hooks/user/useUserGroups'
import {getStorage} from '../../utils/Storage/useStorage'
import {useNavigator} from '../useNavigator'

interface Props extends ScreenBaseProps {}

function GroupListScreen({navigation}: Props) {
    const userSrl = getStorage('userSrl')
    const {groups} = useUserGroups(Number(userSrl))
    const {popToTop} = useNavigator()

    return (
        <ScreenLayout>
            <Button title="홈으로" onPress={popToTop} />
        </ScreenLayout>
    )
}

export default GroupListScreen
