import React from 'react'
import {Button} from 'react-native'
import {ScreenBaseProps} from '..'
import Image from '../../components/Image'
import ScreenLayout from '../../components/ScreenLayout'
import {removeStorage} from '../../utils/Storage/useStorage'

interface Props extends ScreenBaseProps {}

function LogoutScreen(props: Props) {
    const logout = async () => {
        removeStorage('userSrl')
        removeStorage('userJWToken')
    }

    return (
        <ScreenLayout>
            <ScreenLayout.Header title="로그아웃" backType="ARROW" />
            <Image name="Logo" />
            <Button title="로그아웃" onPress={logout} />
        </ScreenLayout>
    )
}

export default LogoutScreen
