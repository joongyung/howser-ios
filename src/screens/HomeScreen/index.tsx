import React, {useState} from 'react'
import {useDefaultModal} from '../../components/Modal/useDefaultModal'
import {useBottomOptionModal} from '../../components/Modal/useBottomOptionModal'
import {ScreenBaseProps} from '..'
import {useNavigator} from '../useNavigator'
import ScreenLayout from '../../components/ScreenLayout'
import {Text} from 'react-native'

interface Props extends ScreenBaseProps {}

function HomeScreen({navigation}: Props) {
    const [id, setId] = useState(1)
    const {navigate} = useNavigator()

    const {open: defaultModal} = useDefaultModal({
        title: '안내드립니다',
        desc: '상차대기로 초기화 하시겠습니까? \n 초기화 시, 이전 상차 정보는 복구되지 않습니다.',
        subDesc: 'e.g. 12층',
        input: {
            placeHolder: '입력하세요.',
        },
    })

    const openDefaultModal = async () => {
        const result = await defaultModal()
        if (result.comfirm) {
            console.log('recieved:', result.data)
        }
    }

    const {open: bottomModal} = useBottomOptionModal({
        title: '물류센터를 선택하세요',
        options: [
            {
                id: 1,
                name: '전체',
            },
            {
                id: 2,
                name: '양지센터',
            },
            {
                id: 3,
                name: '용인센터',
            },
            {
                id: 4,
                name: '이천센터',
            },
        ],
        selectedId: id,
    })

    const openBottomModal = async () => {
        const result = await bottomModal()
        if (result.comfirm) {
            console.log(result.data)
            setId(result.data.id)
        }
    }

    const {open: reciveModal} = useBottomOptionModal({
        title: '하자 옵션을 선택하세요',
        options: [
            {
                id: 1,
                name: '배송중하자발생(시공전)',
            },
            {
                id: 2,
                name: '설치중하자발생(시공후)',
            },
            {
                id: 3,
                name: '배송중하자발생(시공후)',
            },
        ],
        selectedId: id,
    })

    const openReciveModal = async () => {
        const result = await reciveModal()
        if (result.comfirm) {
            console.log(result.data.id)
            setId(result.data.id)
        }
    }

    return (
        <ScreenLayout>
            <ScreenLayout.HomeHeader />
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <Text>HJI</Text>
            <ScreenLayout.Button
                title="테스트 화면"
                onPress={() => navigate('테스트 화면')}
            />
        </ScreenLayout>
    )
}

export default HomeScreen
