import AxiosClient from './AxiosClient'

const client = new AxiosClient()
const instance = client.getInstance()

export const apiGateway = instance
