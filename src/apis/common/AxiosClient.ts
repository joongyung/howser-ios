import axios, {AxiosError, AxiosInstance} from 'axios';
import {Alert} from 'react-native';
import {URLS} from '../../utils/GlobalConfig/data-config';
import {setStorage, removeStorage} from '../../utils/Storage/useStorage';

export interface HowserError {
  error: {
    code: number;
    message: string;
  };
}

export default class AxiosClient {
  private instance: AxiosInstance;

  constructor() {
    this.instance = axios.create({
      baseURL: URLS.BASE_URL,
      timeout: 5000,
    });
    this.setReqInterceptor();
    this.setResInterceptor();
  }

  private setReqInterceptor() {
    this.instance.interceptors.request.use(reqConfig => {
      return reqConfig;
    });
  }

  private setResInterceptor() {
    this.instance.interceptors.response.use(res => {
      const jwt = res.headers['set-cookie']!;

      if (jwt?.length > 0 && jwt[0].includes('Bearer')) {
        setStorage('userJWToken', jwt[0]);
      }

      // 인증 에러
      if (res?.data?.error?.code === 10000) {
        removeStorage('userSrl');
        removeStorage('userJWToken');
        Alert.alert('사용자 정보를 찾을 수 없습니다.');
      }

      return res;
    }, this.errorHandler);
  }

  private errorHandler(err: AxiosError) {
    if (err?.message.includes('timeout')) {
    }
    throw err;
  }

  getInstance() {
    return this.instance;
  }
}
