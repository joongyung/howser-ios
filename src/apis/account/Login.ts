import {Alert} from 'react-native'
import {HowserError} from '../common/AxiosClient'
import sha256 from 'sha256'
import {apiGateway} from '../common/apiGateWay'
import {URLS} from '../../utils/GlobalConfig/data-config'

interface Login extends HowserError {
    data: {
        userSrl: number
    }
}

export async function login(
    id: string,
    password: string,
    deviceId = '4998c14c-f51c-4c4d-bb3f-247a90bfc3401'
) {
    const {data} = await apiGateway.post<Login>(
        URLS.ACCOUNT_ENDPOINT + '/login',
        {
            id,
            password: sha256(password),
            deviceId,
            isAutoLogin: true,
        }
    )

    if (data.error) {
        Alert.alert(data.error.message)
        return undefined
    }

    return data.data.userSrl
}
