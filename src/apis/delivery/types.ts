export interface UserGroups {
    data: {
        srl: number
        id: string
        password: string
        name: string
        email: string
        cellPhone: string
        updDate: string
        regDate: string
        userType: string
        groups: Group[]
    }
}

export interface Group {
    srl: string
    companySrl: number
    name: string
    address: string
    addressDetail: string
    telephone: string
    contactInfo: string
    type: string
    companyName: string
    companyLicenseSn: string
    companyCeoName: string
    companyUnpaid: boolean
    mgrUser: MgrUser
    groupUser: GroupUser
}

export interface GroupUser {
    role: string
    status: string
    regDate: string
}

export interface MgrUser {
    srl: number
    id: string
    name: string
    email: string
    cellphone: string
    status: string
    pwdUpdDate: string
    regDate: string
    updDate: string
    deliveryAreaType: number
}
