import {apiGateway} from '../common/apiGateWay'
import {URLS} from '../../utils/GlobalConfig/data-config'
import {UserGroups} from './types'

export async function getUserGroups(userSrl: number) {
    const {data} = await apiGateway.get<UserGroups>(
        URLS.DELIVERY_ENDPOINT + `/users/${userSrl}/groups`,
        {
            params: {
                groupUserStatus: 'NORMAL',
            },
        }
    )
    return data
}
