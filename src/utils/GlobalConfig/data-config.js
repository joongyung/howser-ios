export const URLS = {
  BASE_URL: `https://dev.howser.co.kr`,
  ACCOUNT_ENDPOINT: '/api/account/v1',
  DELIVERY_ENDPOINT: '/api/delivery/v1',
};
