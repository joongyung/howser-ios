import {MMKVLoader, useMMKVStorage} from 'react-native-mmkv-storage';

const MMKV = new MMKVLoader().initialize();
type KEY = 'userSrl' | 'userJWToken';

// hook
export const useStorage = (key: KEY, defaultValue?: string | number) => {
  const [value, setValue] = useMMKVStorage(key, MMKV, defaultValue);
  return [value, setValue];
};

// ts 모듈 어디서든 사용가능.
export const getStorage = (key: KEY) => {
  const value = MMKV.getString(key, error => {
    if (error === null) {
      return null;
    }

    throw error;
  });

  return value;
};

export const setStorage = (key: KEY, defaultValue: string | number) => {
  MMKV.setString(key, defaultValue.toString());
};

export const removeStorage = (key: KEY) => MMKV.removeItem(key);
