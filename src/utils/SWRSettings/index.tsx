import React, {ReactNode} from 'react';
import {SWRConfig} from 'swr';
import {AppState, AppStateStatus} from 'react-native';

interface Props {
  children: ReactNode;
}

function SWRSettings({children}: Props) {
  return (
    <SWRConfig
      value={{
        provider: () => new Map(),
        isVisible: () => {
          return true;
        },
        initFocus(callback) {
          let appState = AppState.currentState;

          const onAppStateChange = (nextAppState: AppStateStatus) => {
            /* 백그라운드 또는 비활성 모드에서 활성 모드로 다시 시작하는 경우 */
            if (
              appState.match(/inactive|background/) &&
              nextAppState === 'active'
            ) {
              console.log('active');
              callback();
            }
            appState = nextAppState;
          };

          // 앱 상태 변경 이벤트 구독
          const subscription = AppState.addEventListener(
            'change',
            onAppStateChange,
          );

          return () => {
            subscription.remove();
          };
        },
      }}>
      {children}
    </SWRConfig>
  );
}

export default SWRSettings;
