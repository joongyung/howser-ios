import React, {ReactElement, ReactNode, useLayoutEffect} from 'react'
import {SafeAreaView, View} from 'react-native'
import DefaultButton from '../Button/DefaultButton'
import HomeHeader from './HomeHeader'
import Icon from '../Icon'
import styles from './styles'
import {useNavigator} from '../../screens/useNavigator'

interface Props {
    children: ReactNode
}

function ScreenLayout({children}: Props) {
    return <SafeAreaView style={styles.container}>{children}</SafeAreaView>
}

interface ScreenLayoutHomeHeader {}

ScreenLayout.HomeHeader = ({}: ScreenLayoutHomeHeader) => {
    const {navigation} = useNavigator()

    useLayoutEffect(() => {
        navigation.setOptions({
            header: (props) => <HomeHeader {...props} />,
        })
    }, [])
    return <></>
}

interface ScreenLayoutHeader {
    nonShown?: boolean
    title?: string
    backType?: 'ARROW' | 'XBUTTON'
    RightIcon?: ReactElement
    onBackPress?: () => void
    onRightPress?: () => void
}

ScreenLayout.Header = ({
    title,
    backType,
    onBackPress: onBackPressFromProps,
    RightIcon,
    onRightPress,
    nonShown = false,
}: ScreenLayoutHeader) => {
    const {navigation} = useNavigator()
    useLayoutEffect(() => {
        if (nonShown) {
            navigation.setOptions({
                headerShown: false,
            })
        } else {
            const onBackPress = onBackPressFromProps || navigation.pop
            navigation.setOptions({
                headerTitle: title,
                headerLeft:
                    backType === 'XBUTTON'
                        ? () => (
                              <Icon
                                  name="Close"
                                  onPress={() => onBackPress()}
                                  width={20}
                                  height={20}
                              />
                          )
                        : () => (
                              <Icon
                                  name="Arrow"
                                  onPress={() => onBackPress()}
                                  width={20}
                                  height={20}
                              />
                          ),
                headerRight: RightIcon
                    ? () =>
                          React.cloneElement(RightIcon!, {
                              onPress: onRightPress,
                          })
                    : undefined,
            })
        }
    }, [])
    return <></>
}

interface ScreenLayoutButton {
    title: string
    onPress: () => void
}

ScreenLayout.Button = ({title, onPress}: ScreenLayoutButton) => (
    <SafeAreaView style={styles.buttonWrapper}>
        <DefaultButton title={title} onPress={onPress} />
    </SafeAreaView>
)

interface ScreenLayoutButtons {
    prevTitle: string
    nextTitle: string
    onPrevPress: () => void
    onNextPress: () => void
}

ScreenLayout.Buttons = ({
    prevTitle,
    nextTitle,
    onPrevPress,
    onNextPress,
}: ScreenLayoutButtons) => (
    <SafeAreaView style={styles.buttonWrapper}>
        <DefaultButton
            title={prevTitle}
            onPress={onPrevPress}
            buttonStyle={styles.cancleButton}
            fontStyle={styles.cancelFont}
        />
        <DefaultButton
            title={nextTitle}
            onPress={onNextPress}
            buttonStyle={styles.button}
            fontStyle={styles.font}
        />
    </SafeAreaView>
)

export default ScreenLayout
