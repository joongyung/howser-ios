import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    container: {
        height: '100%',
        position: 'relative',
    },
    buttonWrapper: {
        display: 'flex',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
    },
    button: {
        width: '50%',
        borderColor: '#000',
        paddingLeft: 73,
        paddingRight: 73,
        paddingTop: 17,
        paddingBottom: 17,
        backgroundColor: '#1fbdb0',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    font: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#fff',
    },
    cancleButton: {
        width: '50%',
        borderColor: '#000',
        paddingLeft: 73,
        paddingRight: 73,
        paddingTop: 17,
        paddingBottom: 17,
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    cancelFont: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#000',
    },
})
