import {NativeStackHeaderProps} from '@react-navigation/native-stack';
import React from 'react';
import {SafeAreaView, Text} from 'react-native';
import {useNavigator} from '../../../screens/useNavigator';
import Icon from '../../Icon';
import styles from './styles';

interface Props extends NativeStackHeaderProps {}

function HomeHeader({}: Props) {
  const {openDrawer} = useNavigator();

  const pressMenu = () => {
    openDrawer();
  };

  return (
    <SafeAreaView style={styles.header}>
      <Icon name="Menu" width={20} height={20} onPress={pressMenu} />
      <Text>HOWSER</Text>
    </SafeAreaView>
  );
}

export default HomeHeader;
