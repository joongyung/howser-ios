import React from 'react'
import {
    Image as ImageComponent,
    ImageProps,
    Pressable,
    PressableProps,
} from 'react-native'
import * as image from '../../assets/image'

type ImageType = keyof typeof image
interface Props
    extends Omit<ImageProps, 'source'>,
        Pick<PressableProps, 'onPress'> {
    name: ImageType
}

function Image({name, onPress, ...rest}: Props) {
    return (
        <Pressable onPress={onPress}>
            <ImageComponent source={image[name]} {...rest} />
        </Pressable>
    )
}

export default Image
