import React from 'react'
import {Button, Text, View} from 'react-native'
import styles from './styles'

interface Props {
    title: string
    desc: string
    onDefaultModal: () => void
    onWareHouseModal: () => void
    onReciveOptionModal: () => void
}

function Card({
    title,
    desc,
    onDefaultModal,
    onWareHouseModal,
    onReciveOptionModal,
}: Props) {
    return (
        <View style={styles.container}>
            <View style={styles.imgContainer}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <View style={styles.descContainer}>
                <Text numberOfLines={2} style={styles.desc}>
                    {desc}
                </Text>
            </View>
            <View />
            <View>
                <Button title="기본모달" onPress={onDefaultModal} />
                <Button title="물류센터" onPress={onWareHouseModal} />
                <Button title="회수옵션" onPress={onReciveOptionModal} />
            </View>
        </View>
    )
}

export default Card
