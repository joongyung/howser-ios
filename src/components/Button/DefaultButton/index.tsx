import React from 'react';
import {
  StyleProp,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';
import {Text} from 'react-native';
import styles from './styles';

interface Props extends TouchableOpacityProps {
  title: string;
  buttonStyle?: StyleProp<ViewStyle>;
  fontStyle?: StyleProp<TextStyle>;
}

function DefaultButton({title, buttonStyle, fontStyle, ...rest}: Props) {
  return (
    <TouchableOpacity {...rest} style={buttonStyle ?? styles.button}>
      <Text style={fontStyle ?? styles.font}>{title}</Text>
    </TouchableOpacity>
  );
}

export default DefaultButton;
