import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  button: {
    borderColor: '#000',
    paddingLeft: 73,
    paddingRight: 73,
    paddingTop: 17,
    paddingBottom: 17,
    backgroundColor: '#1fbdb0',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
  },
  font: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
});
