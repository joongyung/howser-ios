import React, {useCallback, useLayoutEffect, useMemo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useModal} from './useModal';
import Modal from 'react-native-modal';

interface Option {
  id: number;
  name: string;
}

interface Props {
  title: string;
  options: Option[];
  selectedId: Option['id'];
}

export function useBottomOptionModal({title, options, selectedId}: Props) {
  useLayoutEffect(() => {
    if (options.length < selectedId) {
      throw new Error(
        `[BottomOptionModal] selectedId(${selectedId})값이 주어진 options의 길이(${options.length})보다 큽니다.`,
      );
    }
  }, [options.length, selectedId]);

  const {open} = useModal();
  const isSelectedOption = useMemo(
    () => [styles.contentTitle, styles.selected],
    [],
  );
  const isOption = useMemo(() => [styles.contentTitle], []);

  const openModal = useCallback(() => {
    return open<Option>(({onConfirm, onCancel}) => {
      return (
        <Modal
          isVisible={true}
          onSwipeComplete={onCancel}
          swipeDirection={['down']}
          style={styles.view}>
          <View style={styles.content}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.selectBox}>
              {options.map(option => (
                <Text
                  key={option.id}
                  onPress={() => onConfirm(option)}
                  style={
                    option.id === selectedId ? isSelectedOption : isOption
                  }>
                  {option.name}
                </Text>
              ))}
            </View>
          </View>
        </Modal>
      );
    });
  }, [isOption, isSelectedOption, open, options, selectedId, title]);

  return useMemo(() => ({open: openModal}), [openModal]);
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 16,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#353d4a',
    textAlign: 'center',
  },
  selectBox: {
    marginTop: 40,
    display: 'flex',
    justifyContent: 'flex-start',
  },
  contentTitle: {
    fontSize: 16,
    marginBottom: 30,
    color: '#353d4a',
  },
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  selected: {
    fontWeight: 'bold',
  },
});
