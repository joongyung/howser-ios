import React, {useCallback, useMemo, useRef} from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import Input, {InputHandle} from '../Input';
import {useModal} from './useModal';
import Modal from 'react-native-modal';

interface Props {
  title?: string;
  desc: string;
  subDesc?: string;
  input?: {
    defaultValue?: string;
    placeHolder?: string;
  };
}

export function useDefaultModal({title, desc, subDesc, input}: Props) {
  const {open} = useModal();
  const ref = useRef<InputHandle>(null);

  const openModal = useCallback(() => {
    return open<string>(({onConfirm, onCancel}) => {
      return (
        <Modal isVisible={true} animationInTiming={1} animationOutTiming={1}>
          <View style={styles.content}>
            <View style={styles.topContainer}>
              {title && <Text style={styles.title}>{title}</Text>}
              <Text style={styles.desc}>{desc}</Text>
              {subDesc && <Text style={styles.subDesc}>({subDesc})</Text>}
              {input && (
                <View style={styles.inputCotainer}>
                  <Input
                    ref={ref}
                    style={styles.input}
                    placeholder={input.placeHolder}
                    defaultValue={input.defaultValue}
                  />
                </View>
              )}
            </View>
            <View style={styles.buttonContainer}>
              <Pressable
                onPress={onCancel}
                style={[styles.button, styles.btnDivider]}>
                <Text style={styles.btnTitle}>취소</Text>
              </Pressable>
              <Pressable
                onPress={() => onConfirm(ref.current?.getValue() ?? '')}
                style={styles.button}>
                <Text style={styles.btnComfirmTitle}>확인</Text>
              </Pressable>
            </View>
          </View>
        </Modal>
      );
    });
  }, [desc, input, open, subDesc, title]);

  return useMemo(() => ({open: openModal}), [openModal]);
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  topContainer: {
    width: '100%',
    padding: 30,
    fontStyle: 'normal',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#444444',
    marginBottom: 10,
    textAlign: 'center',
  },
  desc: {
    fontSize: 15,
    color: '#353d4a',
    textAlign: 'center',
  },
  subDesc: {
    fontSize: 12,
    color: '#444444',
    textAlign: 'center',
    marginTop: 5,
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#fbfbfb',
    width: '100%',
  },
  button: {
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 60,
    paddingLeft: 60,
    width: '50%',
    borderTopColor: '#f0f0f1',
    borderTopWidth: 1,
  },
  btnDivider: {
    borderRightColor: '#f0f0f1',
    borderRightWidth: 1,
  },
  btnTitle: {
    fontStyle: 'normal',
    fontSize: 16,
    color: '#353d4a',
  },
  btnComfirmTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#1fbdb0',
  },
  input: {
    width: '100%',
    height: 40,
    padding: 10,
    textAlign: 'center',
  },
  inputCotainer: {
    width: '100%',
    borderBottomColor: '#f2f4f6',
    borderBottomWidth: 1,
    paddingBottom: -10,
    marginTop: 10,
  },
});
