import {
    DrawerContentComponentProps,
    DrawerContentScrollView,
} from '@react-navigation/drawer'
import React, {useMemo} from 'react'
import {Pressable, Text, View} from 'react-native'
import {DRAWER_SCREENS, DRAWER_SCREENS_KEYS} from '../../screens/screenStacks'
import {useNavigator} from '../../screens/useNavigator'
import styles from './styles'

function DrawerNavigator(props: DrawerContentComponentProps) {
    const {closeDrawer, navigate} = useNavigator()

    const pressMenu = (key: keyof typeof DRAWER_SCREENS) => {
        closeDrawer()
        navigate(DRAWER_SCREENS[key].name)
    }

    const MENUS = useMemo(() => DRAWER_SCREENS_KEYS.filter((_, i) => i > 0), [])

    return (
        <View style={styles.container}>
            <DrawerContentScrollView>
                <View style={styles.profileContainer}>
                    <View style={styles.img} />
                    <Text style={styles.nickname}>최준경</Text>
                    <Text style={styles.email}>jkchoe@howser.co.kr</Text>
                </View>
                <View style={styles.menus}>
                    {MENUS.map((key) => (
                        <Pressable key={key} onPress={() => pressMenu(key)}>
                            <View>
                                <Text>{DRAWER_SCREENS[key].nav}</Text>
                            </View>
                        </Pressable>
                    ))}
                </View>
            </DrawerContentScrollView>
        </View>
    )
}

export default DrawerNavigator
