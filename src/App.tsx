import React, {useLayoutEffect} from 'react';
import AppScreenStacks from './screens/screenStacks/AppScreenStacks';
import {ModalProvider} from './components/Modal/useModal';
import SplashScreen from 'react-native-splash-screen';
import SWRSettings from './utils/SWRSettings';

function App() {
  useLayoutEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1000);
  }, []);

  return (
    <SWRSettings>
      <ModalProvider>
        <AppScreenStacks />
      </ModalProvider>
    </SWRSettings>
  );
}

export default App;
