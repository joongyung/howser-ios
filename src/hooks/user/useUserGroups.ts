import useSWR, {useSWRConfig} from 'swr'
import {getUserGroups} from '../../apis/delivery/getUserGroups'

const KEY = '@userGroups'

export function useUserGroups(userSrl: number) {
    const {data: groups, error} = useSWR(userSrl ? KEY : null, () =>
        getUserGroups(userSrl)
    )

    return {
        groups,
        isError: error,
        isLoading: !error && !groups,
    }
}

export function useMutateUserGroups(userSrl: number) {
    const {mutate} = useSWRConfig()
    return {
        mutate: () => mutate(KEY, getUserGroups(userSrl), false),
    }
}
